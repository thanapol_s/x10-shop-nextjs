import { Typography } from "antd";
import React from "react";

type Props = {};

export default function Index({}: Props) {
  return (
    <div>
      <Typography.Title editable level={1} style={{ margin: 0 }}>
        X10 Shop
      </Typography.Title>
    </div>
  );
}
