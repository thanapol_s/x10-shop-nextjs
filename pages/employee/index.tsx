import { Breadcrumb, Button, Card, Col, Row, Space, Table, Tag } from "antd";
import React, { useState } from "react";
import type { ColumnsType } from "antd/es/table";
import { HomeOutlined, UserOutlined } from "@ant-design/icons";
import { PATH_ROUTE } from "@/src/constants/routes";
import Link from "next/link";
import DrawerAdd from "@/src/components/employee/drawer_add";

type Props = {};

export default function Index({}: Props) {
  const [open, setOpen] = useState(false);

  const onOpen = () => {
    setOpen(true);
  };
  interface DataType {
    key: string;
    name: string;
    age: number;
    address: string;
    tags: string[];
  }

  const columns: ColumnsType<DataType> = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Age",
      dataIndex: "age",
      key: "age",
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Tags",
      key: "tags",
      dataIndex: "tags",
      render: (_, { tags }) => (
        <>
          {tags.map((tag) => {
            let color = tag.length > 5 ? "geekblue" : "green";
            if (tag === "loser") {
              color = "volcano";
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <a>Invite {record.name}</a>
          <a>Delete</a>
        </Space>
      ),
    },
  ];

  const data: DataType[] = [
    {
      key: "1",
      name: "John Brown",
      age: 32,
      address: "New York No. 1 Lake Park",
      tags: ["nice", "developer"],
    },
    {
      key: "2",
      name: "Jim Green",
      age: 42,
      address: "London No. 1 Lake Park",
      tags: ["loser"],
    },
    {
      key: "3",
      name: "Joe Black",
      age: 32,
      address: "Sidney No. 1 Lake Park",
      tags: ["cool", "teacher"],
    },
  ];
  return (
    <div style={{ margin: 10 }}>
      <Card>
        <Breadcrumb>
          <Breadcrumb.Item>
            <Link href={PATH_ROUTE.DASHBOARD}>
              <HomeOutlined />
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Employee</Breadcrumb.Item>
        </Breadcrumb>
        <br />
        <Row>
          <Col span={24} style={{ display: "flex", justifyContent: "end" }}>
            <Button
              type="primary"
              onClick={() => {
                onOpen();
              }}
            >
              Add Employee
            </Button>
          </Col>
        </Row>
        <Table columns={columns} dataSource={data} />
      </Card>
      <DrawerAdd setOpen={setOpen} open={open} />
    </div>
  );
}
