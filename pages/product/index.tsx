import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Image,
  Modal,
  Popconfirm,
  Row,
  Space,
  Table,
  Tag,
} from "antd";
import React, { useEffect, useState } from "react";
import type { ColumnsType } from "antd/es/table";
import { HomeOutlined } from "@ant-design/icons";
import { PATH_ROUTE } from "@/src/constants/routes";
import Link from "next/link";
import DrawerAdd from "@/src/components/product/drawer_add";
import Eye from "@2fd/ant-design-icons/lib/Eye";
import CircleEditOutline from "@2fd/ant-design-icons/lib/CircleEditOutline";
import Delete from "@2fd/ant-design-icons/lib/Delete";
import axios, { HttpStatusCode } from "axios";
import { _DELETE, _GET } from "@/src/utils/axios";

type Props = {};

export default function Index({}: Props) {
  const [open, setopen] = useState(false);
  const [data, setData] = useState<DataType[]>([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [valueModal, setValueModal] = useState<any>();
  const [mode, setMode] = useState("add");
  const [modelEdit, setModelEdit] = useState<any>();
  const onAddProduct = () => {
    setopen(true);
    setMode("add");
  };

  useEffect(() => {
    getApi();
  }, []);

  const getApi = () => {
    _GET("http://localhost:4000/products").then((response) => {
      console.log(response);
      let data: any[] = response.data;
      setData(
        data.map<DataType>((item,index) => {
          return {
            key: index+1,
            id: item.id,
            p_name: item.p_name,
            p_type: item.p_type,
            p_color: item.p_color,
            p_price: item.p_price,
            p_description: item.p_description,
            p_size: item.p_size,
            p_status: item.p_status,
            file: []
          };
        })
      );
    });
  };

  const showModal = (values: any) => {
    setValueModal(values);
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };
  const onEditProduct = (value: any) => {
    setModelEdit(value);
    setMode("update");
    setopen(true);
  };
  interface DataType {
    id: number;
    p_name: string;
    p_type: string;
    p_color: string;
    p_price: string;
    p_description: string;
    p_size: string;
    p_status: string;
    file: any[];
  }

  const columns: ColumnsType<DataType> = [
    {
      title: "No.",
      dataIndex: "key",
      key:"key"
    },
    {
      title: "Name",
      dataIndex: "p_name",
      key: "p_name",
      render: (text) => <a>{text}</a>,
    },
    {
      title: "Type",
      dataIndex: "p_type",
      key: "p_type",
    },
    {
      title: "Color",
      dataIndex: "p_color",
      key: "p_color",
    },
    {
      title: "Price",
      dataIndex: "p_price",
      key: "p_price",
    },
    {
      title: "Description",
      dataIndex: "p_description",
      key: "p_description",
    },
    {
      title: "Size",
      dataIndex: "p_size",
      key: "p_size",
      render: (_, { p_size }) => (
        <>
          <Tag color={"green"}>{p_size.toUpperCase()}</Tag>
        </>
      ),
    },
    {
      title: "Status",
      dataIndex: "p_status",
      key: "p_status",
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Eye onClick={() => showModal(record)} style={{ color: "blue" }} />
          <CircleEditOutline
            style={{ color: "orange" }}
            onClick={() => onEditProduct(record)}
          />
          <Popconfirm
            title="Sure to delete?"
            onConfirm={() => handleDelete(record.id)}
          >
            <Delete style={{ color: "red" }} />
          </Popconfirm>
        </Space>
      ),
    },
  ];

  const handleDelete = (key: number) => {
    _DELETE(`http://localhost:4000/products/${key}`).then((response) => {
      if (response.status == HttpStatusCode.Ok) {
        getApi();
      } else {
        console.log(response);
      }
    });
  };

  return (
    <div style={{ margin: 10 }}>
      <Card>
        <Breadcrumb>
          <Breadcrumb.Item>
            <Link href={PATH_ROUTE.DASHBOARD}>
              <HomeOutlined />
            </Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Product</Breadcrumb.Item>
        </Breadcrumb>
        <br />
        <Row>
          <Col span={24} style={{ display: "flex", justifyContent: "end" }}>
            <Button
              type="primary"
              onClick={() => {
                onAddProduct();
              }}
            >
              Add Product
            </Button>
          </Col>
        </Row>
        <Table columns={columns} dataSource={data} />
        <Modal
          title="Product Preview"
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleOk}
        >
          <>
            {valueModal &&
              valueModal.file.map((element: any) => {
                return (
                  <Image
                    src={element.image_url}
                    key={element.id}
                    width="100%"
                  />
                );
              })}
          </>
        </Modal>
      </Card>
      <DrawerAdd
        open={open}
        setOpen={setopen}
        isSubmit={getApi}
        mode={mode}
        modelEdit={modelEdit}
      ></DrawerAdd>
    </div>
  );
}
