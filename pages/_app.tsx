import "@/styles/globals.css";
import type { AppProps } from "next/app";
import "antd/dist/reset.css";
import LayoutMain from "@/src/components/layouts/layout-main";
import { ConfigProvider, theme } from "antd";
import { useEffect, useState } from "react";

export default function App({ Component, pageProps }: AppProps) {
  const [currentTheme, setCurrentTheme] = useState(false);

  const lightTheme = {
    colorPrimary: "#ff1764",
    colorPrimaryBg: "#ffe6ea",
    colorPrimaryBgHover: "#ffbac8",
    colorPrimaryBorder: "#ff91ab",
    colorPrimaryBorderHover: "#ff6991",
    colorPrimaryHover: "#ff4079",
    colorPrimaryActive: "#d90955",
    colorPrimaryTextHover: "#ff4079",
    colorPrimaryText: "#ff1764",
    colorPrimaryTextActive: "#d90955",
  };
  const darkTheme = {
    colorPrimaryBg: "#e6f4ff",
    colorPrimary: "#1777dc",
    colorPrimaryBgHover: "#112945",
    colorPrimaryBorder: "#15375b",
    colorPrimaryBorderHover: "#15497e",
    colorPrimaryHover: "#3c95e8",
    colorPrimaryActive: "#165496",
    colorPrimaryTextHover: "#3888d3",
    colorPrimaryText: "#1768be",
    colorPrimaryTextActive: "#16549",
    colorBgBase: "#000000",
    colorBgContainer: "#141414",
    colorBgElevated: "#1f1f1f",
    colorBgLayout: "#000000",
    colorBgSpotlight: "#424242",
    colorBgMask: "rgba(0, 0, 0, 0.45)",
    colorText: "rgba(255, 255, 255, 0.85)",
    colorTextSecondary: "rgba(255, 255, 255, 0.65)",
    colorTextTertiary: "rgba(255, 255, 255, 0.45)",
    colorTextQuaternary: "rgba(255, 255, 255, 0.25)",
    colorBorder: "#424242",
    colorBorderSecondary: "#303030",
    colorFill: "rgba(255, 255, 255, 0.18)",
    colorFillSecondary: "rgba(255, 255, 255, 0.12)",
    colorFillTertiary: "rgba(255, 255, 255, 0.08)",
    colorFillQuaternary: "rgba(255, 255, 255, 0.04)",
  };
  return (
    <ConfigProvider
      theme={{
        token: currentTheme ? darkTheme : lightTheme,
      }}
    >
      <LayoutMain setCurrentTheme={setCurrentTheme}>
        <Component {...pageProps} />
      </LayoutMain>
    </ConfigProvider>
  );
}
