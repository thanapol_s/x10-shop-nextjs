import axios from "axios";
import { IFromResponse } from "../constants/types";

const setHeader = () => {
  const headers = {
    "Content-Type": "application/json",
  };
  return headers;
};
const setHeaderUploadFile = () => {
  const headers = {
    "Content-Type": "multipart/form-data",
  };
  return headers;
};

export const _GET = async (
  url: string,
  param?: any
): Promise<IFromResponse<any>> => {
  return new Promise((resolve, reject) => {
    axios.get(url).then(
      (res) => {
        resolve(res.data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};

export const _POST = async (
  url: string,
  param?: any
): Promise<IFromResponse<any>> => {
  const headers = setHeader();

  return new Promise((resolve, reject) => {
    axios.post(url, param, { headers: headers }).then(
      (res) => {
        resolve(res.data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};

export const _POSTFILE = async (
  url: string,
  param?: any
): Promise<IFromResponse<any>> => {
  const headers = setHeaderUploadFile();

  return new Promise((resolve, reject) => {
    axios.post(url, param, { headers: headers }).then(
      (res) => {
        resolve(res.data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};

export const _PATCHFILE = async (
  url: string,
  param?: any
): Promise<IFromResponse<any>> => {
  const headers = setHeaderUploadFile();

  return new Promise((resolve, reject) => {
    axios.patch(url, param, { headers: headers }).then(
      (res) => {
        resolve(res.data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};

export const _DELETE = async (
  url: string,
  param?: any
): Promise<IFromResponse<any>> => {
  const headers = setHeader();

  return new Promise((resolve, reject) => {
    axios.delete(url, { headers: headers }).then(
      (res) => {
        resolve(res.data);
      },
      (error) => {
        resolve(reTurnError(error));
      }
    );
  });
};

const reTurnError = (error: any) => {
  //   if (error?.respones?.data?.status === 401) {
  //     const errJwt = {
  //       result: false,
  //       status: 401,
  //       message: "กรุณา Login เข้าสู่ระบบอีกครั้ง",
  //       data: "",
  //     };
  //     return errJwt;
  //   } else
  if (error?.response?.data) {
    return error.response.data;
  } else {
    return error;
  }
};
