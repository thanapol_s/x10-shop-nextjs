import {
  MenuFoldOutlined,
} from "@ant-design/icons";
import WhiteBalanceSunny from '@2fd/ant-design-icons/lib/WhiteBalanceSunny'
import MoonWaxingCrescent from '@2fd/ant-design-icons/lib/MoonWaxingCrescent'
import { Avatar, Button, Col, Layout, Row, Space, Switch, theme } from "antd";
import React, { useState } from "react";

type Props = {
  setCurrentTheme:any
};

const {useToken} = theme
export default function LayoutHeader({
  setCurrentTheme
}: Props) {

  const onChange = (checked: boolean) => {
    setCurrentTheme(checked)
  };
  return (
    <Layout.Header style={{ backgroundColor: useToken().token.colorBgBase }}>
      <Row>
        <Col
          span={24}
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <div>
            <MenuFoldOutlined />
          </div>

          <div>
            <Space>
              <span>Thanapol Sommanapan</span>
              <Avatar style={{ backgroundColor: "#f56a00" }}>TS</Avatar>
              <Switch
              unCheckedChildren={<WhiteBalanceSunny />}
              checkedChildren={<MoonWaxingCrescent />}
              onChange={onChange}/>
            </Space>
          </div>
        </Col>
      </Row>
    </Layout.Header>
  );
}
