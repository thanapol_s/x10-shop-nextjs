import React from "react";
import { Layout } from "antd";
import LayoutMenu from "./layout-menu";
import LayoutHeader from "./layout-header";
import LayoutContent from "./layout-content";
type Props = {
  children: React.ReactNode;
  setCurrentTheme:any;
};

export default function LayoutMain({children,setCurrentTheme}: Props) {
  return (
    <Layout style={{ height: "100vh" }}>
      <LayoutMenu/>
      <Layout>
        <LayoutHeader setCurrentTheme={setCurrentTheme}/>
        <LayoutContent>{children}</LayoutContent>
      </Layout>
    </Layout>
  );
}
