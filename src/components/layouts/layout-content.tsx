import { Layout, theme } from "antd";
type Props = {
  children: React.ReactNode;
};

export default function LayoutContent({ children }: Props) {
  return <Layout.Content>{children}</Layout.Content>;
}
