import {
  Button,
  Drawer,
  Space,
  Form,
  Row,
  Col,
  Input,
  Select,
  Upload,
  UploadProps,
  UploadFile,
} from "antd";
import { RcFile } from "antd/es/upload";
import axios, { HttpStatusCode } from "axios";
import React, { useEffect, useState } from "react";

type Props = {
  open: boolean;
  setOpen: any;
  isSubmit: Function;
  mode: string;
  modelEdit: any;
};

export default function DrawerAdd({
  open,
  setOpen,
  isSubmit,
  mode,
  modelEdit,
}: Props) {
  const [form] = Form.useForm();
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [fileListDelete, setFileListDelete] = useState<any>([]);

  useEffect(() => {
    if (mode == "update") {
      form.setFieldsValue({
        p_name: modelEdit.p_name,
        p_type: modelEdit.p_type,
        p_price: modelEdit.p_price,
        p_color: modelEdit.p_color,
        p_size: modelEdit.p_size,
        p_status: modelEdit.p_status,
        p_description: modelEdit.p_description,
      });
      modelEdit.file.map((element: any, index: string) => {
        element.uuid = index;
        element.url = element.image_url;
      });
      setFileList(modelEdit.file);
    }
  }, [mode, modelEdit]);

  const onClose = () => {
    setOpen(false);
    onReset();
    setFileList([]);
  };

  const onReset = () => {
    form.resetFields();
  };

  const onSubmit = () => {
    form.submit();
  };

  const onSave = async (values: any) => {
    const formData = new FormData();
    formData.append("p_name", values.p_name);
    formData.append("p_type", values.p_type);
    formData.append("p_color", values.p_color);
    formData.append("p_price", values.p_price);
    formData.append("p_size", values.p_size);
    formData.append("p_status", values.p_status);
    formData.append("p_description", values.p_description);
    for (let i = 0; i < fileList.length; i++) {
      formData.append("files", fileList[i].originFileObj as RcFile);
    }
    await axios
      .post("http://localhost:4000/products/create", formData)
      .then((response) => {
        console.log(response);
        if (response.data.status == HttpStatusCode.Ok) {
          isSubmit();
          onClose();
        }
      });
  };

  const onUpdate = async (values: any) => {

    const formDataUpdate = new FormData();
    formDataUpdate.append("id", modelEdit.id);
    formDataUpdate.append("p_name", values.p_name);
    formDataUpdate.append("p_type", values.p_type);
    formDataUpdate.append("p_color", values.p_color);
    formDataUpdate.append("p_price", values.p_price);
    formDataUpdate.append("p_size", values.p_size);
    formDataUpdate.append("p_status", values.p_status);
    formDataUpdate.append("p_description", values.p_description);
    formDataUpdate.append("fileDelete",JSON.stringify(fileListDelete));
    for (let i = 0; i < fileList.length; i++) {
      if (fileList[i]?.originFileObj) {
        formDataUpdate.append(
          "files",
          fileList[i].originFileObj as RcFile,
          "test"
        );
      }
    }
    await axios
      .patch(`http://localhost:4000/products/${modelEdit.id}`, formDataUpdate)
      .then((response) => {
        if (response.data.status == HttpStatusCode.Ok) {
          isSubmit();
          onClose();
        }
      });
  };

  const onChange: UploadProps["onChange"] = ({ fileList: newFileList }) => {
    setFileList(newFileList);
  };

  const onRemove = (value: any) => {
    let data = [...fileListDelete];
    data.push({ "id": value.id });
    setFileListDelete(data);
  };

  const onPreview = async (file: UploadFile) => {
    let src = file.url as string;
    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj as RcFile);
        reader.onload = () => resolve(reader.result as string);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };

  return (
    <>
      <Drawer
        title={mode === "add" ? "Create a new product" : "Update a product"}
        width={720}
        onClose={onClose}
        open={open}
        bodyStyle={{ paddingBottom: 80 }}
        footer={
          <div style={{ display: "flex", justifyContent: "end" }}>
            <Space>
              <Button onClick={onClose}>Cancel</Button>
              <Button onClick={onSubmit} type="primary">
                Submit
              </Button>
            </Space>
          </div>
        }
      >
        <Form
          form={form}
          layout="vertical"
          onFinish={mode === "add" ? onSave : onUpdate}
        >
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="p_name"
                label="Name"
                rules={[
                  { required: true, message: "Please enter product name" },
                ]}
              >
                <Input placeholder="Please enter product name" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="p_type"
                label="Type"
                rules={[
                  { required: true, message: "Please enter product type" },
                ]}
              >
                <Input placeholder="Please enter product type" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="p_color"
                label="Color"
                rules={[{ required: true, message: "Please enter color" }]}
              >
                <Input placeholder="Please enter color" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="p_price"
                label="Price"
                rules={[
                  { required: true, message: "Please enter product price" },
                ]}
              >
                <Input placeholder="Please enter product price" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="p_size"
                label="Size"
                rules={[{ required: true, message: "Please enter size" }]}
              >
                <Input placeholder="Please enter size" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="p_status"
                label="status"
                rules={[{ required: true }]}
              >
                <Select placeholder="Select a option status" allowClear>
                  <Select.Option value="active">Active</Select.Option>
                  <Select.Option value="inactive">InActive</Select.Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name="p_description"
                label="Description"
                rules={[
                  { required: true, message: "Please enter description" },
                ]}
              >
                <Input.TextArea placeholder="Please enter description" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item label="file">
                <Upload
                  listType="picture-card"
                  fileList={fileList}
                  onChange={onChange}
                  onPreview={onPreview}
                  onRemove={onRemove}
                  accept="image/png, image/jpeg"
                >
                  {fileList.length < 3 && "+ Upload"}
                </Upload>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Drawer>
    </>
  );
}
