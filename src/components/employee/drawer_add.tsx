import {
  Button,
  Col,
  DatePicker,
  Drawer,
  Form,
  Input,
  Row,
  Select,
  Space,
} from "antd";
import axios from "axios";
import React, { useEffect, useState } from "react";

type Props = {
  setOpen: any;
  open: boolean;
};

export default function DrawerAdd({ setOpen, open }: Props) {

  useEffect(() => {
    getApi();
  }, [])
  
  const getApi = () => {
    axios.get("https://raw.githubusercontent.com/kongvut/thai-province-data/master/api_province_with_amphure_tambon.json").then((response) => {
      console.log(response.data);
    });
  }

  const onClose = () => {
    setOpen(false);
  };
  return (
    <Drawer
      title="Create a new Employee"
      width={720}
      onClose={onClose}
      open={open}
      bodyStyle={{ paddingBottom: 80 }}
      footer={
        <div style={{ display: "flex", justifyContent: "end" }}>
          <Space>
            <Button onClick={onClose}>Cancel</Button>
            <Button onClick={onClose} type="primary">
              Submit
            </Button>
          </Space>
        </div>
      }
    >
      <Form layout="vertical">
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              name="firstName"
              label="first name"
              rules={[{ required: true, message: "Please enter first name" }]}
            >
              <Input placeholder="Please enter first name" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="lastName"
              label="last name"
              rules={[{ required: true, message: "Please enter last name" }]}
            >
              <Input placeholder="Please enter last name" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="phoneNumber"
              label="phone number"
              rules={[{ required: true, message: "Please enter phone number" }]}
            >
              <Input type="tel" placeholder="Please enter phone number" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="address"
              label="Address"
              rules={[{ required: true, message: "Please enter address" }]}
            >
              <Input type="tel" placeholder="Please enter address" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="province"
              label="Province"
              rules={[{ required: true, message: "Please enter address" }]}
            >
              <Input type="tel" placeholder="Please enter address" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="district"
              label="District"
              rules={[{ required: true, message: "Please enter district" }]}
            >
              <Input type="tel" placeholder="Please enter district" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="sub_district"
              label="Sub District"
              rules={[{ required: true, message: "Please enter sub district" }]}
            >
              <Input type="tel" placeholder="Please enter sub district" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="post_code"
              label="Post Code"
              rules={[{ required: true, message: "Please enter post code" }]}
            >
              <Input type="tel" placeholder="Please enter post code" />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
}
